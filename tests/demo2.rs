use macro_rules_attribute::derive;
use uniserde::Build;

#[derive(Build!)]
struct A {
    b: B,
    c: B,
}

#[derive(Build!)]
struct B {}

#[test]
fn demo() {}
