use crate::{
    builtins::walker::hint::Meta,
    protocol::{Implementer, Protocol},
    symbol::Symbol,
};

pub enum Tagged {}

pub trait TaggedWalker<'ctx> {
    fn kind(&mut self) -> Symbol;

    fn walk_tag(&mut self, visitor: &mut dyn Implementer<'ctx>) -> Result<(), ()>;

    fn walk_value(&mut self, visitor: &mut dyn Implementer<'ctx>) -> Result<(), ()>;
}

pub trait Object<'ctx> {
    fn visit(&mut self, walker: &mut dyn TaggedWalker<'ctx>) -> Result<(), ()>;
}

impl Protocol for Tagged {
    type Object<'a, 'ctx: 'a> = &'a mut dyn Object<'ctx>;
}

pub struct Known {
    pub kind_available: Option<bool>,
}

pub struct Hint {
    pub kind: Option<Symbol>,
}

impl Meta for Tagged {
    type Known<'a, 'ctx: 'a> = Known;

    type Hint<'a, 'ctx: 'a> = Hint;
}
