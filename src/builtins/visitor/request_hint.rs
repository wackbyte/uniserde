//! Protocol for requesting a visitor give a hint to walker.
///
/// Some walkers don't know what the data they are walking actually represents.
/// This protocol allows such walkers to request the visitor to give a hint of what
/// it is expecting.
use crate::protocol::{Implementer, Protocol};

/// Protocol for requesting a visitor give a hint.
pub enum RequestHint {}

/// Object implementing the [`RequestHint`] protocol.
pub trait RequestHintObject<'ctx> {
    /// Call this to request a hint.
    ///
    /// `hints` is expected to be the walker. This is what the visitor (`Self`)
    /// will call to give a hint using the
    /// [`Hint`][crate::builtins::walker::hint::Hint] protocol.
    fn request_hint(&mut self, hints: &mut dyn Implementer<'ctx>) -> Result<(), ()>;
}

impl Protocol for RequestHint {
    type Object<'a, 'ctx: 'a> = &'a mut dyn RequestHintObject<'ctx>;
}
