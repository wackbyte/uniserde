
use crate::{
    builtins::walker::hint::Meta,
    protocol::{Implementer, Protocol},
};

pub enum Sequence {}

#[derive(PartialEq, Eq, PartialOrd, Ord, Copy, Clone, Debug)]
pub enum Status {
    Done,
    Continue,
}

pub trait SequenceWalker<'ctx> {
    fn next(&mut self, visitor: &mut dyn Implementer<'ctx>) -> Result<Status, ()>;
}

pub trait Object<'ctx> {
    fn visit(&mut self, walker: &mut dyn SequenceWalker<'ctx>) -> Result<(), ()>;
}

impl Protocol for Sequence {
    type Object<'a, 'ctx: 'a> = &'a mut dyn Object<'ctx>;
}

#[derive(Default)]
pub struct Known {
    pub len: (usize, Option<usize>),
}

pub struct Hint {
    pub len: (usize, Option<usize>),
}

impl Meta for Sequence {
    type Known<'a, 'ctx: 'a> = Known;

    type Hint<'a, 'ctx: 'a> = Hint;
}
