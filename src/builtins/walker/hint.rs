//! Protocol for giving a hint to a walker.
//!
//! Sometimes a walker has multiple protocols it could use,
//! this module gives a protocol by which a visitor can give a hint
//! to the walker about what it is expecting.

use crate::protocol::{Implementer, Protocol};

/// Protocol for giving a hint to a walker.
///
/// A hint is for a particular protocol `P`.
pub struct Hint<P: Meta>(P);

/// Meta information for the hint.
///
/// This gives the visitor more information to work from when selecting a hint.
pub trait Meta: Protocol {
    /// Information known by the walker.
    ///
    /// This should be information easy to get without changing the state of the walker
    /// in an irreversable way.
    type Known<'a, 'ctx: 'a>;

    /// Extra information the visitor can give to the walker about what it is expecting.
    type Hint<'a, 'ctx: 'a>;
}

/// Object implementing the [`Hint`] protocol.
pub trait Object<'ctx, P: Meta> {
    /// Hint to the walker to use the `P` protocol.
    ///
    /// This should only be called once per [`RequestHint`].
    fn hint(
        &mut self,
        visitor: &mut dyn Implementer<'ctx>,
        hint: P::Hint<'_, 'ctx>,
    ) -> Result<(), ()>;

    /// Ask the walker for information about it's support of the protocol.
    fn known(&mut self, hint: &P::Hint<'_, 'ctx>) -> Result<P::Known<'_, 'ctx>, ()>;
}

impl<P: Meta> Protocol for Hint<P> {
    type Object<'a, 'ctx: 'a> = &'a mut dyn Object<'ctx, P>;
}
