use crate::{build::protocols, implementer, protocol::ImplementerExt, walk, Builder};

use super::IncompleteValue;

pub struct OwnedBuilder<T> {
    value: Option<T>,
}

impl<T> Default for OwnedBuilder<T> {
    fn default() -> Self {
        Self { value: None }
    }
}

impl<'ctx, T: 'static> Builder<'ctx> for OwnedBuilder<T> {
    type Error = IncompleteValue;

    type Value = T;

    fn as_visitor(&mut self) -> &mut dyn crate::protocol::Implementer<'ctx> {
        self
    }

    fn build(self) -> Result<Self::Value, Self::Error> {
        self.value.ok_or(IncompleteValue)
    }
}

implementer! {
    impl['ctx, T: 'static] OwnedBuilder<T> = [
        protocols::owned::Owned<T>,
        protocols::hint::RequestHint,
    ];
}

impl<'ctx, T: 'static> protocols::hint::RequestHintObject<'ctx> for OwnedBuilder<T> {
    fn request_hint(
        &mut self,
        hints: &mut dyn crate::protocol::Implementer<'ctx>,
    ) -> Result<(), ()> {
        if let Some(interface) =
            hints.interface_for::<walk::protocols::hint::Hint<protocols::owned::Owned<T>>>()
        {
            interface.as_object().hint(self, ())
        } else {
            Ok(())
        }
    }
}

impl<'ctx, T: 'static> protocols::owned::Object<'ctx, T> for OwnedBuilder<T> {
    fn visit(&mut self, value: T) -> Result<(), ()> {
        self.value = Some(value);
        Ok(())
    }
}
