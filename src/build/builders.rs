mod owned;

pub use owned::*;

#[derive(thiserror::Error, Debug)]
#[error("The value is not complete.")]
pub struct IncompleteValue;
