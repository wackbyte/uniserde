// pub mod builders;
// pub mod protocols;

use crate::protocol::Implementer;

/// A buildable type.
pub trait Build<'ctx>: Sized {
    /// The builder that can be used to build a value of `Self`.
    type Builder: Builder<'ctx, Value = Self>;
}

/// Builder for a type.
///
/// The `'ctx` lifetime is some lifetime that is longer than the walker.
/// As such, the built value can borrow from other data with a `'ctx` lifetimes.
///
/// A builder allows creating a value of a type [`Self::Value`].
/// The way to use a builder is as follows.
/// - Call [`Default::default()`] to create an instance of the builder.
/// - Call [`Self::as_visitor()`] and give it to a walker's
///     [`walk()`][crate::walk::Walker::walk]. The walker will then fill
///     the builder with data from it's walk.
/// - Call [`Self::build()`] to finish building the value and get any errors
///     that happened during filling it with data.
pub trait Builder<'ctx>: Default {
    /// Error that can happen during filling the builder with data.
    type Error;

    /// Type to be built.
    type Value;

    /// Get the builder as a visitor that a walker can use.
    ///
    /// This is expected to just be `self`.
    fn as_visitor(&mut self) -> &mut dyn Implementer<'ctx>;

    /// Finish the value.
    ///
    /// If an error happened with the builder during the walk
    /// it will be reported here.
    fn build(self) -> Result<Self::Value, Self::Error>;
}
