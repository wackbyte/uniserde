use crate::{
    protocol::VisitorMissingProtocol,
    walk::{Walk, WalkMut, WalkOnce},
    Visitor, Walker,
};

use super::{reference::RefWalker, reference_mut::MutWalker};

impl<'ctx> WalkOnce<'ctx> for &'ctx str {
    type Error = VisitorMissingProtocol;

    type Value = ();

    #[inline]
    fn walk_once(self, visitor: &mut dyn Visitor<'ctx>) -> Result<Self::Value, Self::Error> {
        self.walk(visitor)
    }
}

impl<'borrow, 'ctx> WalkMut<'borrow, 'ctx> for &'ctx str {
    #[inline]
    fn walk_mut(
        &'borrow mut self,
        visitor: &mut dyn Visitor<'ctx>,
    ) -> Result<Self::Value, Self::Error> {
        self.walk(visitor)
    }
}

impl<'borrow, 'ctx> Walk<'borrow, 'ctx> for &'ctx str {
    #[inline]
    fn walk(&'borrow self, visitor: &mut dyn Visitor<'ctx>) -> Result<Self::Value, Self::Error> {
        RefWalker::new(*self).walk_once(visitor)
    }
}

impl<'ctx> WalkOnce<'ctx> for &'ctx mut str {
    type Error = VisitorMissingProtocol;

    type Value = ();

    #[inline]
    fn walk_once(self, visitor: &mut dyn Visitor<'ctx>) -> Result<Self::Value, Self::Error> {
        MutWalker::new(self).walk_once(visitor)
    }
}

impl<'ctx> WalkMut<'ctx, 'ctx> for &'ctx mut str {
    #[inline]
    fn walk_mut(
        &'ctx mut self,
        visitor: &mut dyn Visitor<'ctx>,
    ) -> Result<Self::Value, Self::Error> {
        MutWalker::new(*self).walk_once(visitor)
    }
}

impl<'ctx> Walk<'ctx, 'ctx> for &'ctx mut str {
    #[inline]
    fn walk(&'ctx self, visitor: &mut dyn Visitor<'ctx>) -> Result<Self::Value, Self::Error> {
        RefWalker::new(*self).walk_once(visitor)
    }
}
