use crate::{
    build::{builders::OwnedBuilder, Build},
    walk::walkers::OwnedCloneWalker,
    Walk,
};

#[derive(thiserror::Error, Debug)]
#[error("The value is complete.")]
pub struct IncompleteValue;

impl<'ctx> Walk<'ctx> for bool {
    type Walker = OwnedCloneWalker<Self>;
}

impl<'ctx> Build<'ctx> for bool {
    type Builder = OwnedBuilder<Self>;
}
