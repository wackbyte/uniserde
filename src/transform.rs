use crate::{
    build::{Build, Builder},
    walk::Walk,
    Walker,
};

#[derive(thiserror::Error, Debug)]
pub enum Either<A, B> {
    #[error(transparent)]
    A(A),

    #[error(transparent)]
    B(B),
}

pub fn from<'ctx, U, T>(
    value: T,
) -> Result<U, Either<<T::Walker as Walker<'ctx>>::Error, <U::Builder as Builder<'ctx>>::Error>>
where
    T: Walk<'ctx>,
    U: Build<'ctx>,
{
    let mut builder = U::Builder::default();
    T::Walker::from(value)
        .walk(builder.as_visitor())
        .map_err(Either::A)?;
    builder.build().map_err(Either::B)
}
