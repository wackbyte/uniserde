//! Interface for interfaces.
//!
//! ## Design
//! The design of protocols is based on an idea found in the
//! [`gdbstub`](https://docs.rs/gdbstub/latest/gdbstub/target/ext/index.html) crate.
//! This idea is of so called inlinable dyn extension traits.
//! However, in the form given in `gdbstub` they can't be used for arbitrary interfaces.
//! The main trait still needs to know about all the possible protocols.
//! That is where this module comes in.
//!
//! This module implements a technique we name dynamic inlinable dyn extension traits (DIDETs).
//! DIDETs adds one more layer to IDETs. Instead of a trait that knows all the possible protocols,
//! we have a single trait [`Implementer`] that allows looking up an extension trait
//! using a type ID. This may seem like it defeats the purpose of IDETs, that being to
//! make them inlinable. However, it turns out LLVM (the optimizer) is able to see
//! through this style of runtime reflection. As such, we still gain the benefits of
//! IDETs but with more flexability.
//! Protocols can now be defined in *any* crate and used between arbitrary crates.
//!
//! A protocol is a special trait that can participate as a DIDET. The only thing needed
//! for a protocol is an associated trait object. Because we need to use the
//! [`TypeId`][core::any::TypeId] of a protocol to perform reflection, we can't just use
//! the trait object itself as the protocol type. Instead an uninhabited type is used
//! as a marker for the trait.
//!
//! We then "implement" a protocol for a type by using [`Implementation`]. This provides
//! a mapping from `T` to the protocol's trait object.
//! By itself, [`Implementation`] is not enough for DIDET. A type also needs to implement
//! [`Implementer`] which allows looking up a particular [`Implementation`] trait object
//! from a [`ProtocolId`].
//!
//! The implementation of DIDETs defined by this module allows [`Implementer`] to be object safe.
//! This is done via the help of the [`AnyImpl`] type. This is not required for the core
//! idea of DIDETs.

/// Type nameable trait.
///
/// Traits cannot by named in the type system. Trait objects can. However,
/// trait objects are not always `'static` so don't always have a
/// [`TypeId`][core::any::TypeId]. This trait provides a way to name a trait object
/// in the type system and with a `TypeId` even if it contains lifetimes.
///
/// [`Protocol`] should be implemented on a marker type that is `'static`.
/// [`Protocol`] then gives a mapping from that marker type to a trait object
/// given by [`Protocol::Object`]. Its recommended to use uninhabited marker
/// types when possible as the marker never needs to exist as a value.
///
/// The `'ctx` lifetime is a lifetime the trait object can contain. The `'a` lifetime
/// is the lifetime of a reference to the trait object. As such, the trait object
/// needs to live for at least `'a`.
///
/// ```
/// // Some trait we want to use as a protocol.
/// trait MyTrait<'ctx> {}
///
/// // Type to name MyTrait in the type system.
/// enum MyTraitProtocol {}
///
/// // By implementing this we map MyTraitProtocol to MyTrait.
/// impl Protocol for MyTraitProtocol {
///     type Object<'a, 'ctx: 'a> = dyn MyTrait<'ctx> + 'a;
/// }
/// ```
pub trait Protocol<'ctx>: TypeNameable<'ctx> {
    /// The trait object form of the trait.
    ///
    /// This should be of the form `dyn Trait<'ctx> + 'a` where `'a` sets the
    /// required lifetime of the trait object.
    ///
    /// Note, it is possible (and safe) to put non-trait object type here, but
    /// it is likely to not play well with [`AnyObject`].
    type Object<'a>: ?Sized where 'ctx: 'a;
}

pub trait Implementer<'ctx> {
    fn interface(&self, id: LtTypeId<'ctx>) -> Option<&dyn LtAny<'ctx>>;
}

pub trait ImplementerMut<'ctx> {
    fn interface_mut(&mut self, id: LtTypeId<'ctx>) -> Option<&mut dyn LtAny<'ctx>>;
}

/// Extension trait for getting the implementation of a protocol.
pub trait ImplementerMutExt<'ctx>: ImplementerMut<'ctx> {
    /// Get an implementation given a protocol type.
    ///
    /// This wraps [`Implementer::interface`] and [`AnyImpl::downcast`].
    /// If [`Implementer::interface`] returns a [`AnyImpl`] for the wrong protocol then a panic is
    /// generated.
    fn interface_mut_for<'a, P: Protocol<'ctx>>(&'a mut self) -> Option<&'a mut P::Object<'a>>
    where
        'ctx: 'a;
}

impl<'ctx, T: ImplementerMut<'ctx> + ?Sized> ImplementerMutExt<'ctx> for T {
    fn interface_mut_for<'a, P: Protocol<'ctx>>(&'a mut self) -> Option<&'a mut P::Object<'a>>
    where
        'ctx: 'a,
    {
        match self.interface_mut(LtTypeId::of::<P>()) {
            Some(interface) => match interface.downcast_mut::<P::Object<'a>>() {
                Some(implementation) => Some(implementation),
                None => panic!(
                    "unexpected protocol implementation: `{:?}`, expected: `{:?}`",
                    interface.id(),
                    P::id()
                ),
            },
            None => None,
        }
    }
}

/// Implement [`Implementer`] and [`Implementation`] for a set of protocols.
#[doc(hidden)]
#[macro_export]
macro_rules! implementer {
    {
        impl[$ctx:lifetime $($generic:tt)*] $name:ty = [$($protocol:ty),* $(,)?];
    } => {
        impl<$ctx $($generic)*> $crate::protocol::Implementer<$ctx> for $name {
            #[inline]
            fn interface(
                &mut self,
                id: $crate::protocol::ProtocolId
            ) -> ::core::option::Option<$crate::protocol::AnyObject<'_, $ctx>> {
                match id {
                    $(id if id == $crate::protocol::ProtocolId::of::<$protocol>()
                        => Some($crate::protocol::AnyObject::new::<$protocol>(self)),)*
                    _ => None
                }
            }
        }
    };
}
#[doc(inline)]
pub use implementer;

use crate::any::{LtTypeId, LtAny, TypeNameable};

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn implementer_macro() {
        struct X<T>(T);

        enum Y {}

        trait Z {}

        impl Protocol for Y {
            type Object<'a, 'ctx> = dyn Z + 'a;
        }

        implementer! {
            impl['ctx, T: Clone] X<T> = [
                Y
            ];
        }

        impl<T: Clone> Z for X<T> {}
    }
}
