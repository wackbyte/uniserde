use core::any::TypeId;

use super::Protocol;

/// ID of a protocol.
///
/// An ID also includes the protocol's type name for easier debugging.
#[derive(Copy, Clone)]
pub struct ProtocolId {
    /// Type ID of the protocol type.
    id: fn() -> TypeId,

    /// Name of the protocol type.
    name: fn() -> &'static str,
}

impl ProtocolId {
    /// Get the ID of a protocol.
    pub const fn of<P: Protocol>() -> Self {
        Self {
            id: || core::any::TypeId::of::<P>(),
            name: || core::any::type_name::<P>(),
        }
    }

    /// Type ID of the protocol.
    ///
    /// This is used for comparision.
    fn id(&self) -> TypeId {
        (self.id)()
    }

    /// Type name of the protocol.
    ///
    /// This is used for debugging purposes.
    fn name(&self) -> &'static str {
        (self.name)()
    }
}

impl core::fmt::Debug for ProtocolId {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.debug_struct("ProtocolId")
            .field("id", &self.id())
            .field("name", &self.name())
            .finish()
    }
}

impl core::fmt::Display for ProtocolId {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        // Just print the type name.
        self.name().fmt(f)
    }
}

impl PartialEq for ProtocolId {
    fn eq(&self, other: &Self) -> bool {
        self.id() == other.id()
    }
}

impl Eq for ProtocolId {}

impl PartialOrd for ProtocolId {
    fn partial_cmp(&self, other: &Self) -> Option<core::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for ProtocolId {
    fn cmp(&self, other: &Self) -> core::cmp::Ordering {
        self.id().cmp(&other.id())
    }
}

impl core::hash::Hash for ProtocolId {
    fn hash<H: core::hash::Hasher>(&self, state: &mut H) {
        self.id().hash(state);
    }
}
