use crate::protocol::ProtocolId;

mod owned;
mod owned_clone;

mod serde;

pub use owned::*;
pub use owned_clone::*;

#[derive(thiserror::Error, Debug)]
#[error("The visitor is missing protocol: {0}")]
pub struct MissingProtocol(pub ProtocolId);
