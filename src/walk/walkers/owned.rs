use crate::{
    build::protocols,
    protocol::{Implementer, ImplementerExt, ProtocolExt},
    Walker,
};

use super::MissingProtocol;

pub struct OwnedWalker<T> {
    value: T,
}

impl<T> OwnedWalker<T> {
    pub const fn new(value: T) -> Self {
        Self { value }
    }
}

impl<T> From<T> for OwnedWalker<T> {
    fn from(value: T) -> Self {
        Self::new(value)
    }
}

impl<'ctx, T: 'static> Walker<'ctx> for OwnedWalker<T> {
    type Error = (MissingProtocol, T);

    type Value = ();

    fn walk(self, visitor: &mut dyn Implementer<'ctx>) -> Result<Self::Value, Self::Error> {
        if let Some(interface) = visitor.interface_for::<protocols::owned::Owned<T>>() {
            let _ = interface.as_object().visit(self.value);
            Ok(())
        } else {
            Err((
                MissingProtocol(protocols::owned::Owned::<T>::id()),
                self.value,
            ))
        }
    }
}
